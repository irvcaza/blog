#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Irving Cabrera'
SITENAME = 'El blog sin bautizar'
SITEURL = 'https://irvcaza.gitlab.io/blog'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/Mexico_City'

DEFAULT_LANG = 'es'

THEME = 'theme'

PLUGIN_PATHS = ["plugins"]
PLUGINS = ["tag_cloud"]

DEFAULT_DATE_FORMAT = '%d/%m/%Y'

AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''
YEAR_ARCHIVE_SAVE_AS = 'archive/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'archive/{date:%Y}/{date:%m}/index.html'
YEAR_ARCHIVE_URL = 'archive/{date:%Y}/index.html'
MONTH_ARCHIVE_URL = 'archive/{date:%Y}/{date:%m}/index.html'
ARCHIVES_SAVE_AS = 'archives.html'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Hecho con Pelican', 'http://getpelican.com/'),
         )

# Social widget
SOCIAL = (('Gitlab', 'https://gitlab.com/irvcaza'),
          ('LinkedIn', 'https://www.linkedin.com/in/irvingcabrera/'),
          ('Twitter', 'https://twitter.com/irvcaza'),
          ('Reddit','https://www.reddit.com/user/irvcz'))

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
#LOAD_CONTENT_CACHE = False