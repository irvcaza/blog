Title: HTTP a HTTPS fácil con docker-compose y Traefik 2
Date: 2020-08-13
Category: self-hosting
Slug: HTTPaHTTPS
summary: En este artículo quiero mostrar cómo puedes usar Traefik para que todo el trafico se a traves de HTTPS utilizando docker-compose para configurarlo.
tags: docker, docker-compose, Traefik, 

![Traefik]({static}/images/Traefik-entrypoints.png)

¡Hola! Este es el primer artículo de blog de lo que espero será una serie de cómo tener un montón de servicios en internet sin gastar (casi) nada.

En este artículo veremos cómo hacer que nuestros servicios se accedan de manera automática a través de HTTPS. Sin importar si escribes la dirección http:// o con https://

Antes de comenzar quiero agradecer al equipo de Traefik quienes me dieron ese pequeño empujoncito para escribir este post. Es la razón por la que publico este post antes que otros que puedan considerar más básicos, como son ¿Como consigo un servidor?, ¿De donde saco servicios? ¿Como se usa docker?. Son un equipo fabuloso y ahora verán que Traefik es una herramienta espectacular.

## ¿De verdad necesitas HTTPS?

Si te estás preguntando si de verdad necesitas una conexión https depende principalmente del tipo de servicios que tengas en tu servidor. ¿Manejas datos sensibles? Como tarjetas de crédito, correos electrónicos, nombres direcciones, etc. entonces **necesitas** una conección mediante https. Algunos servicios como el gestor de contraseñas Bitwarden no funcionará de manera correcta a menos que te conectes por https. Si quizás tienes un blog estático, aquí es opcional si usar uno u otro, pero recuerda que el uso de HTTPS mejora tu posicionamiento en el buscador de Google, así también los navegadores ahora muestran las páginas como no seguras si usan solo http.

Veremos que es muy fácil configurarlo y una vez hecho, te olvidas de hacerlo de nuevo. 

En este tutorial usaremos la autoridad de certificados Let's Encrypt la cual es muy sencilla de utilizar y además gratuita. Ahora veremos como configurar traefik 

## Configuraciones 
### Configuración estática
En el archivo de docker- compose de traefik tienes que agregar los siguientes comandos en la sección de command, después veremos lo que significan

```
command:
      - "--entrypoints.web.address=:80" #(1)
      - "--entryPoints.websecure.address=:443"  #(2)
      - "--certificatesResolvers.le.acme.email=micorreo@example.com" #(3)
      - "--certificatesResolvers.le.acme.storage=acme.json" #(4)
      - "--certificatesResolvers.le.acme.httpChallenge.entryPoint=web" #(5)
```
* (1) Establecer un punto de entrada llamado “web” en el puerto 80 (el puerto de HTTP)
* (2) Establecer un punto de entrada llamado “websecure” en el puerto 443 (el puerto de HTTPS)
* (3) Establecer una autoridad de certificados llamada “le” (Let’s Encrypt) un proveedor con el proveedor ACME, con el correo micorreo@example.com . Por supuesto debes cambiarlo por tu correo para poder recibir información importante acerca de tus certificados. 
* (4) para la autoridad le, guardar certificados en el archivo acme.json
* (5) para la autoridad le, mostraremos que el dominio es de nuestra propiedad con  un httpChallenge el cual va a ser en el punto de entrada “web”

Con esto tenemos configurado nuestra autoridad de certificados. Recuerda agregar los puertos 80 y 443 para que sean accesibles desde el contenedor. 

### Configuración dinámica 
Ahora, cuando lancemos un contenedor solo tenemos que agregar las siguientes líneas en la sección de labels, por ejemplo si lanzamos un servicio de WhoAmI

```
labels:
    - "traefik.http.routers.whoami.entrypoints=websecure" #(6)
    - "traefik.http.routers.whoami.tls=true" #(7)
    - "traefik.http.routers.whoami.tls.certresolver=le" #(8)
```

* (6) en el router “whoami” (debes cambiarlo por el nombre de tu router) escuchara en el punto de entrada “websecure”
* (7) el router “whomi” tendrá tls, es decir será a través de https
* (8) la autoridad para resolver el certificado será la que definimos como “le”

Ahora podemos acceder a nuestro sitio mediante https. El problema es que si intentamos acceder mediante http nos marcará un error ya que, no establecimos el entrypoint web. Esto se puede solucionar agregando el label

```
labels:
    - "traefik.http.routers.whoami.entrypoints=web" 
```

### Redireccion a HTTPS
Sin embargo, si queremos que se acepte sólo mediante HTTPS entonces tendremos que usar un middleware, de preferencia en los labels de traefik mismo, para que esté disponible desde que ejecutamos traefik. Usamos la siguiente configuración. 

```
labels:
      - "traefik.http.routers.http-catchall.rule=hostregexp(`{host:.+}`)" #(9)
      - "traefik.http.routers.http-catchall.entrypoints=web" #(10)
      - "traefik.http.routers.http-catchall.priority=20" #(11)
      - "traefik.http.routers.http-catchall.middlewares=redirect-to-https" #(12)
      - "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https" #(13)
```
* (9) Creamos un router llamado http-catchall el cual acepta cualquier dirección
* (10) el punto de entrada va a ser web (http)
* (11) establecemos una prioridad en 20 para que este router tenga más prioridad que cualquier dirección (entre mas grande el número mayor será la prioridad) 
* (12) para esta router establecemos el middleware llamado redirect-to-https
* (13) lo que hace el middleware redirect-to-https es cambiar a https

Con esto ya tenemos configurados nuestros servicios en https. 

## Preguntas frecuentes
Este tutorial es bastante corto por lo que pueden haber quedado muchas dudas o cosas no fueron claras

* **¿Cuanto cuesta el certificado para HTTPS?**
Aunque hay muchas compañías que cobran por este servicio, sin embargo Let’s encrypt es una organización sin fines de lucro que lo hace de manera gratuita. De cualquier modo, si lo usas, te invito a que hagas un donación para mantener este proyecto. 
* **¿Tengo que renovar mis certificados?**
Traefik se encarga de renovar los certificados cuando falten 30 días para que expiren. De cualquier manera, asegúrate de proporcionar un correo válido para que te notifiquen si algún certificado está próximo a expirar. 
* **¿Tengo que crear un certificado para cada dominio/subdominio?**
Traefik se encarga automáticamente de crear el certificado si este no existe, basado en la regla de los routers.
* **¿Hay algún límite para certificados?**
En este tutorial se crea un certificado para cada subdominio, debes tener en cuenta los límites de la [API de Let’s Encrypt](https://letsencrypt.org/es/docs/rate-limits/) los cuales aunque son bastante permisivos pueden meterte en problemas si no eres cuidadoso.


Para más información puedes revisar la ayuda en la [documentación de trafik](https://docs.traefik.io/https/acme/) o en distintas comunidades como [traefik en reddit](https://www.reddit.com/r/Traefik/), tambien puedes contactarme en la sección de social. 
