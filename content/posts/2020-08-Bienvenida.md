Title: Bienvenida
Date: 2020-08-10
Category: Personal
Slug: bienvenida
summary: ¡Bienvenido a mi blog!


¡Hola! Y bienvenido a mi blog. Este artículo es el primero (oficialmente)
de este blog y tiene dos propósitos. Uno, que este blog no se vea completamente
solo. Y segundo, para darte la bienvenida.

Este blog tiene un corte personal, con tutoriales, artículos, opiniones y tips
sobre temas de mi interés. Principalmente tecnología, software libre,
self-hosting, programación, ciencia, estadística, ciencia de datos,
machine learnig, motos... de todo un poco.

Por el momento mis publicaciones serán unidireccionales, yo escribo y tu lees, eso no implica que no
aprecie tu retroalimentación. Siempre puedes contactarme en el apartado social.

Por último quiero agradecerte por tomarte el tiempo de leer esto, espero que
disfrutes de este blog.